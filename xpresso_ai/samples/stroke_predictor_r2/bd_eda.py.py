#!/usr/bin/env python
# coding: utf-8

# In[12]:


from xpresso.ai.core.data.distributed.exploration.distributed_dataset_explorer import Explorer
from xpresso.ai.core.data.distributed.automl.distributed_structured_dataset import DistributedStructuredDataset
from xpresso.ai.core.data.visualization.visualization import Visualization
from xpresso.ai.core.data.connections import Connector
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
import databricks.koalas as ks


# In[31]:


input_config = {
    "type": "FS",
    "dataset_type": "distributed",
    "delimiter": ",",
    "path": "/xpresso-data/projects/stroke_predictor_v2/input/train/train.csv"
}
print(input_config)


# In[32]:


d_kdf = DistributedStructuredDataset()
d_kdf.import_dataset(user_config=input_config)
print('Import success!')


# In[33]:


print("Columns:\n",d_kdf.data.columns,'\nShape:\n',d_kdf.data.shape)


# In[54]:


# login here 
get_ipython().run_line_magic('run', '-i ../../../xpresso.ai/scripts/python/xp_login.py qa gagandeep.chhabra')


# In[40]:


controller_factory = VersionControllerFactory()
request_manager = controller_factory.get_version_controller('hdfs')
print('request manager ready')
versioning_repo_name = 'stroke_predictor_v2'
versioning_branch_name = 'develop'


# In[42]:


branch_info = {
    'repo_name': versioning_repo_name,
    'branch_name': versioning_branch_name
}
request_manager.create_branch(**branch_info)


# In[43]:


kwargs = {
    "repo_name": versioning_repo_name,
    "branch_name": versioning_branch_name,
    "dataset": d_kdf,
    "description": "Heart stroke predictor initial data made available"
}
print(kwargs)
response = request_manager.push_dataset(**kwargs)


# In[44]:


print(type(response))
print(response)


# In[45]:


d_kdf.data.isna().sum()


# In[46]:


d_kdf.data = d_kdf.data.dropna()


# In[60]:


d_kdf.data.isna().sum()
print("Columns:\n",d_kdf.data.columns,'\nShape:\n',d_kdf.data.shape)


# In[49]:


exp = Explorer(d_kdf)
exp.understand()


# In[51]:


help(exp.explore_univariate)
exp.explore_univariate(to_excel=True)


# In[55]:


kwargs = {
                "repo_name": versioning_repo_name,
                "branch_name": versioning_branch_name,
                "dataset": d_kdf,
                "description": "cleaned the data from na values and performed univariate analysis"
            }
print(kwargs)
response_cleaned = request_manager.push_dataset(**kwargs)


# In[56]:


print("response_cleaned_commit", response_cleaned)


# In[62]:


kwargs = {
    "repo_name": versioning_repo_name,
    "branch_name": versioning_branch_name,
    "commit_id": response_cleaned[0]
}
pulled_cleaned_dataset = request_manager.pull_dataset(**kwargs)


# In[63]:


print("Columns:\n", pulled_cleaned_dataset.data.columns,'\nShape:\n', pulled_cleaned_dataset.data.shape)


# In[65]:


pulled_cleaned_dataset.data.isna().sum()


# In[66]:


from xpresso.ai.core.data.exploration.render_exploration import RenderExploration
RenderExploration(pulled_cleaned_dataset).render_univariate()


# In[70]:


from xpresso.ai.core.data.visualization.visualization import Visualization
vis = Visualization().get_visualizer(pulled_cleaned_dataset)
vis.render_univariate(report=True, output_path = "./reports/pulled_cleaned_dataset/")


# In[ ]:





# In[ ]:




